const needle = require('needle');

const authenticationMiddleware = {
    url: null
}

module.exports = {
    init: function(url) {
        authenticationMiddleware.url = url;
    },
    validate: function(req, res, next) {
        const token = req.header('Authorization');
        console.log(token);
        needle.post(authenticationMiddleware.url, {token}, function(err, response) {
            console.log("auth middleware", err);
            if(response.body.user) {
                req.body.user = response.body.user;
                next();
            } else {
                return res.status(500).json({success: false, loggedIn: false});
            }
        });
    }
}
